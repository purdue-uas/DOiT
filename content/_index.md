## Welcome DOiT! Camp!

### Welcome to Purdue!

The School of Aviation and Transportation Technology's UAS department looks forward to hosting you this Friday, 04 March 2022! We're going to build, fly, simulate, and of course give away some Purdue UAS swag!

### Before you Arrive

We have lots planned, but little time! So that we hit the skies flyin' please take a few moments to download the DJI Go4 app. Here's how:

1. Download the DJI Go4 App on your iPhone or Android Phone

[![Apple App Store](/img/appleapp.png)](https://apps.apple.com/us/app/dji-go-4/id1170452592)

[![Google Play Store](/img/googleapp.png)](https://play.google.com/store/apps/details?id=dji.go.v4)

2. Then follow these steps to create an account:
   a. Click the sign up button
   
   ![djigo4_1](/img/djigo4_1.jpg)
   
   b. Enter your email and the security code
   
   ![djigo4_2](/img/djigo4_2.PNG)
   
   c. Enter password and agree to app terms
   
   ![djigo4_3](/img/djigo4_3.PNG)
   
   d. Enter your name, gender, and country
   
   ![djigo4_4](/img/djigo4_4.PNG)
   
   e. Nice work! 🙌
   
   ![djigo4_5](/img/djigo4_5.PNG)

If you encounter any issues with the download, contact Prof. Rose via the links at the bottom of the page. 